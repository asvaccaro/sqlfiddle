CREATE TABLE Contacts (
  Id int NOT NULL AUTO_INCREMENT,
  Email varchar(255) NOT NULL UNIQUE,
  FirstName varchar(100) NOT NULL,
  LastName varchar(255) NOT NULL,
  PRIMARY KEY (Id)
);

CREATE TABLE States(
  Id int NOT NULL AUTO_INCREMENT,
  Name varchar(255) NOT NULL,
  PRIMARY KEY (Id)
);

CREATE TABLE Addresses(
    Id int NOT NULL AUTO_INCREMENT,
    Address varchar(255),
    City varchar(255),
    StateId int,
    Zip varchar(5),
    ContactId int NOT NULL,
    PRIMARY KEY (Id),
    FOREIGN KEY (ContactId) REFERENCES Contacts(Id)
);

CREATE TABLE Timespans(
   Id int NOT NULL AUTO_INCREMENT,
   MoveInDate date NOT NULL,
   MoveOutDate date,
   AddressId int NOT NULL,
   PRIMARY KEY (Id),
   FOREIGN KEY (AddressId) REFERENCES Addresses(Id)
);


 
-- Data

INSERT INTO States (Name)
VALUES
 ('New York'),
 ('Florida'),
 ('California');
 
 
INSERT INTO Contacts (Email, FirstName, LastName)
VALUES
 ('asvaccaro@live.com', 'Andrew', 'Vaccaro'),
 ('test@test.com', 'John', 'Doe');
 
 
INSERT INTO Addresses(Address, City, StateId, Zip, ContactId)
VALUES
  ('123 Fake Street', 'Buffalo', 1, '14220', 1),  
  ('123 Main Street', 'Fort Myers', 2, '41142', 2);

  
INSERT INTO Timespans(MoveInDate, MoveOutDate, AddressId)
VALUES
  ("2005-01-05", '2006-01-05', 1),  
  ("2008-05-07", '2009-05-07', 1),  
  ("2009-05-07", '2010-05-07', 2);



-----------------

  
-- Get all addresses for asvaccaro@live.com
SELECT A.Address, A.City, S.Name, A.Zip, C.Email, C.FirstName, C.LastName 
FROM Addresses as A
JOIN Contacts AS C ON A.ContactId = C.Id
JOIN States as S ON A.StateId = S.Id;
WHERE C.Email = 'asvaccaro@live.com'; -- using name for example


-- List all timespans for an address
SELECT A.Address, T.MoveInDate, T.MoveOutDate
FROM Timespans as T
JOIN Addresses AS A ON A.Id = T.AddressId
WHERE A.Address = '123 Fake Street'; -- using name for example


-- List all timespans for an for account
SELECT C.Email, T.MoveInDate, T.MoveOutDate
FROM Addresses as A
JOIN Contacts as C ON A.ContactId = C.Id
JOIN Timespans AS T ON T.AddressId = A.Id
WHERE C.Email = 'asvaccaro@live.com'; -- using name for example


-- Lookup Address's state
SELECT S.Name
FROM States as S
JOIN Addresses AS A ON A.StateId = S.Id
WHERE A.Address = '123 Fake Street'; -- using name for example




